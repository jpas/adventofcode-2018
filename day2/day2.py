#!/usr/bin/env python3
import sys
from collections import Counter
from itertools import combinations


def main(paths):
    for path in paths:
        with open(path) as f:
            two, three = 0, 0
            ids = []
            for line in f:
                line = line.strip()
                ids.append(line)
                c = set(Counter(line).values())
                two += 2 in c
                three += 3 in c
            print(f'{path}: {two * three}')

            for A, B in combinations(ids, 2):
                err = None
                for i, (a, b) in enumerate(zip(A, B)):
                    if a != b:
                        if err is not None:
                            break
                        err = i
                else:
                    S = ''.join(c for j, c in enumerate(A) if j != err)
                    print(f'{path}: {S}\n')
                    break


if __name__ == '__main__':
    exit(main(sys.argv[1:]))

#!/usr/bin/env python3
import sys
from itertools import cycle


def clean(line):
    return int(line.strip())


def main(paths):
    for path in paths:
        with open(path) as f:
            changes = cycle(map(clean, f))
            freqs = set()
            freq = 0
            for change in changes:
                if freq in freqs:
                    print(f'{path}: {freq}')
                    break
                freqs.add(freq)
                freq += change



if __name__ == '__main__':
    exit(main(sys.argv[1:]))
